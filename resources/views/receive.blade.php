<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous"></script>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Test</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        <div class="content">
            <div class="m-b-md">
                New notification will be alerted realtime!
            </div>
        </div>
    </div>

    <!-- receive notifications -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>

    <script>
    Echo.private('message')
        .listen('.my-event', (e) => {
            alert(e);
        });
    // Echo.channel('message');
    // channel.listen('.my-event', function(data) {
    //     $('#test_message').append('<p>' + data.message.content + '</p>')
    // });
    </script>
    <!-- receive notifications -->
</body>

</html>