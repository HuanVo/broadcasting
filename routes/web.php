<?php

use Illuminate\Support\Facades\Route;
use App\Events\SendNotification;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/counter', function () {
    return view('counter');
});

Route::get('/send', function () {
    return view('sendmessage');
});

Route::post('/send', function () {
    $text = request()->text;
    event(new SendNotification($text));
});

Route::get('/receive', function () {
    return view('receive');
});

Route::get('message/send', 'SendMessageController@index')->name('send');
Route::post('message/postMessage', 'SendMessageController@sendMessage')->name('postMessage');

// Route::get('message/index', 'MessageController@index');
//  Route::get('message/send', 'MessageController@send');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');